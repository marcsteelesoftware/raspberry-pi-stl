''' 
	Script for monitoring LiquidSoap and updating the display
	Author: Marc Steele
	Date: April 2014
'''

import pifacecad
import threading
import Queue
import subprocess
from netifaces import interfaces, ifaddresses, AF_INET
import urllib2
import json
import time
import atexit

'''
	Script Wide Variables
'''

now_playing_url = '[MUSICSTATS URL GOES HERE]'
now_playing_artist_text = '<NO DATA>'
now_playing_title_text = '<NO DATA>'
current_ip_text = ''
liquidsoap_log_file = '/var/log/liquidsoap/city_stl.log'
on_backup = False
piface = None

screen_count = 3
current_screen = -1

'''
        Helper Functions
'''

# Current IP address

def getCurrentIp():
	addresses = [i['addr'] for i in ifaddresses('eth0').setdefault(AF_INET, [{'addr':'No IP Address...'}] )]
	return addresses[0]

# Now playing

def nowPlayingThread():
	
	while 1:

		# Pull the info from the server

		try:

			global now_playing_artist_text
			global now_playing_title_text
			now_playing_response = urllib2.urlopen(now_playing_url)
			now_playing_data = json.load(now_playing_response)
			now_playing_title_text = now_playing_data['song']['name']
			now_playing_artist_text = "by %s" % (now_playing_data['song']['given_artist'])

		except Exception, e:
			print "Ran into an error pulling the now playing data: %s" % e

		# Wait for a minute before updating again

		time.sleep(60)

# Current IP address

def currentIPThread():

	while 1:

	
		global current_ip_text
		current_ip_text = "Current Address:\n%s" % getCurrentIp()
		time.sleep(60)

# Liquidsoap Monitoring


def liquidsoapThread():

	global liquidsoap_log_file
	global on_backup

	liq_proc = subprocess.Popen(["tail", "-f", liquidsoap_log_file], stdout=subprocess.PIPE)

	while 1:

		line = liq_proc.stdout.readline()

		if 'Switch to http' in line:
			print 'Flipped to main source...'
			on_backup = False
			subprocess.call("/home/pi/mailsend_recover.pl")
		if 'Switch to single' in line:
			print 'ON BACKUP!'
			on_backup = True
			subprocess.call("/home/pi/mailsend.pl")

# Bail out process

def exit_handler():

	global piface
	piface.lcd.clear()
	piface.lcd.write("Rebooting codec!")
	print "Bailing out..."	

'''
	Main Logic
'''

# Register the bail out process

atexit.register(exit_handler)

# Setup the PiFace

piface = pifacecad.PiFaceCAD()
piface.lcd.backlight_on()
piface.lcd.cursor_off()
piface.lcd.blink_off()

piface.lcd.write("NHR IP STL Codec\n%s" % getCurrentIp())

# Setup the various background threads

threading.Thread(target=nowPlayingThread).start()
threading.Thread(target=currentIPThread).start()
threading.Thread(target=liquidsoapThread).start()
time.sleep(5)

# Display loop

while 1:


	if (on_backup):

		piface.lcd.clear()
		piface.lcd.write("!!!ON  BACKUP!!!\n%s" % getCurrentIp())
		time.sleep(10)

	else:

		current_screen = (current_screen + 1) % screen_count

		if (current_screen == 0):
			
			piface.lcd.clear()
			piface.lcd.write("%s\n%s" % (now_playing_title_text, now_playing_artist_text))

			overspill_length = max(len(now_playing_title_text), len(now_playing_artist_text)) - 16

			if overspill_length <= 0:
				time.sleep(10)
			else:

				sleep_time = max(20 / (overspill_length + 1), 1)
				for i in range(0, overspill_length + 1):
					time.sleep(sleep_time)
					piface.lcd.move_left()

		elif (current_screen == 1):
			piface.lcd.clear()
			piface.lcd.write('Streaming NHR\nAAC-HE 64kbps')
			time.sleep(10)

		elif (current_screen == 2):
			piface.lcd.clear()
			piface.lcd.write(current_ip_text)
			time.sleep(10)

